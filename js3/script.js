const person = {
    name: 'John',
    age: 30,
    gender: 'male'
};

const newPerson = Object.assign({}, person);

newPerson.age = 40;

console.log(person);
console.log(newPerson);









const car = {
    make: 'Toyota',
    model: 'Corolla',
    year: 150
};
function printCarInfo(car) {
    console.log(`Make: ${car.make}, Model: ${car.model}, Year: ${car.year}`);


    if (car.year < 2001) {
        console.log('Машина занадто стара.');
    }
}
printCarInfo(car);







const ser = 'JavaScript is a high-level programming language';


function countWords(str) {
    return ser.trim().split(/\s+/).length;
}

console.log(countWords(ser));






const str = 'JavaScript is awesome!';

function reverseString(str) {
    return str.split('').reverse().join('');
}


console.log(reverseString(str));









const sao = 'JavaScript is awesome!';

function reverseWordsInString(str) {
    return sao.split(' ').map(word => word.split('').reverse().join('')).join(' ');
}

console.log(reverseWordsInString(sao));